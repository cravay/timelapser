package com.gmail.michaelschertenleib.timelapser;

import javafx.scene.control.Alert;

/**
 * utility methods for displaying alerts
 */
public class AlertHelper {
    /**
     * utility method to show an info alert
     *
     * @param title
     * @param message
     */
    public static void showInfo(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * utility method to show an error alert
     *
     * @param title
     * @param message
     */
    public static void showError(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
