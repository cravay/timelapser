package com.gmail.michaelschertenleib.timelapser;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * this is the controller-class for the progress-window
 */
public class LoadingController {
    /**
     * this progress-bar indicates the progress of the conversion
     */
    @FXML
    private ProgressBar progressBar;

    /**
     * this label is used to display additional information
     */
    @FXML
    private Label label;

    /**
     * set the progress percentage
     *
     * @param value
     */
    public void setProgress(double value) {
        progressBar.setProgress(value);
    }

    /**
     * set the string value
     *
     * @param value
     */
    public void setText(String value) {
        label.setText(value);
    }
}
