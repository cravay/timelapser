package com.gmail.michaelschertenleib.timelapser;

import javafx.stage.DirectoryChooser;

import java.io.File;
import java.util.Arrays;

/**
 * this class is used to open folders containing images
 */
public class FolderSelector {
    /**
     * let the user select a folder
     *
     * @return a list of image URIs
     */
    public static String[] selectFilesList() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("select folder");
        File selectedDirectory = chooser.showDialog(Main.primaryStage);

        if (selectedDirectory == null) {
            return null;
        }

        File[] files = selectedDirectory.listFiles((dir, name) -> name.toUpperCase().matches(".*\\.(BMP|GIF|JPE?G|PNG)"));

        return Arrays.stream(files).map(file -> file.toURI().toString()).toArray(String[]::new);
    }
}
