package com.gmail.michaelschertenleib.timelapser;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * main class
 */
public class Main extends Application {
    /**
     * make the primary-stage available everywhere
     */
    public static Stage primaryStage;

    /**
     * entry point
     *
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        primaryStage = stage;

        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        primaryStage.setTitle("Timelapser");
        primaryStage.setScene(new Scene(root, 640, 480));
        primaryStage.show();

    }
}
