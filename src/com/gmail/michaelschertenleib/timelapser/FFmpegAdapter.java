package com.gmail.michaelschertenleib.timelapser;

import javafx.util.Callback;

import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * FFmpegAdapter is used to call ffmpeg
 */
public class FFmpegAdapter {

    /**
     * this class is used by the progress-callback
     */
    public static class ProgressInformation {
        /**
         * progress-message
         */
        public String text;

        /**
         * progress (between 0 and 1)
         */
        public double progress;

        /**
         * constructor
         *
         * @param text
         * @param progress
         */
        ProgressInformation(String text, double progress) {
            this.text = text;
            this.progress = progress;
        }
    }

    /**
     * use FFmpeg to create a movie containing all the uris
     *
     * @param uris
     * @param frameRate
     * @param progressCallback this gets called for every image
     */
    public static void render(String[] uris, int frameRate, Callback<ProgressInformation, Void> progressCallback) {
        (new Thread(() -> {
            Runtime runtime = Runtime.getRuntime();
            Process process;

            try {
                process = runtime.exec("ffmpeg -y -framerate " + frameRate + " -f image2pipe -i - -pix_fmt yuv420p output.mp4");

                OutputStream stdin = process.getOutputStream();

                // the error stream needs to be closed (or read)
                // otherwise the process gets stuck
                process.getErrorStream().close();


                for (int i = 0; i < uris.length; i++) {
                    Path file = Paths.get(new URI(uris[i]));

                    progressCallback.call(new ProgressInformation("processing " + file.getFileName(),
                            (double) i / (uris.length)));

                    stdin.write(Files.readAllBytes(file));
                    stdin.flush();
                }

                stdin.close();
                process.waitFor();

                progressCallback.call(new ProgressInformation("done", 1));

            } catch (Exception exception) {
                AlertHelper.showError("error", "failed to create movie. make sure FFmpeg is installed.");
            }
        })).start();
    }
}
