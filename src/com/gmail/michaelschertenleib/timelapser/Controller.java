package com.gmail.michaelschertenleib.timelapser;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * the main controller
 */
public class Controller implements Initializable {
    /**
     * the bar with the pictures at the bottom of the window
     */
    @FXML
    private HBox pictureBar;

    /**
     * the container where the large image is shown
     */
    @FXML
    private StackPane bigImage;

    /**
     * the view for the large image
     */
    @FXML
    private ImageView imagePreview;

    /**
     * label that tells the user to open a folder
     */
    @FXML
    private Label infoText;

    /**
     * spinner to select the frames per second
     */
    @FXML
    private Spinner fpsSpinner;

    /**
     * array of file uris present in the picture bar
     */
    private String[] activeFileNames;

    /**
     * index of the selected image
     */
    private int activeIndex;

    /**
     * index of the first image of the time-lapse
     */
    private int startIndex;

    /**
     * index of the last image of the time-lapse
     */
    private int endIndex;

    /**
     * initialize the view
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imagePreview.fitWidthProperty().bind(bigImage.widthProperty());
        imagePreview.fitHeightProperty().bind(bigImage.heightProperty());
    }

    /**
     * open a folder with images and populate the image bar
     */
    public void openFolder() {
        infoText.setVisible(false);

        activeFileNames = FolderSelector.selectFilesList();

        if (activeFileNames == null || activeFileNames.length == 0) {
            return;
        }

        pictureBar.getChildren().clear();

        // populate the picture-bar
        for (int i = 0; i < activeFileNames.length; i++) {
            ImagePreviewView imagePreviewView = new ImagePreviewView(activeFileNames[i], i);
            imagePreviewView.fitHeightProperty().bind(pictureBar.heightProperty());

            pictureBar.getChildren().add(imagePreviewView);
        }

        // show first image
        selectImage((ImagePreviewView) pictureBar.getChildren().get(0));

        // set initial range
        startIndex = 0;
        endIndex = activeFileNames.length - 1;
    }

    /**
     * handle clicks on images in the image bar
     *
     * @param event
     */
    public void handleImageClick(Event event) {
        if (event.getTarget() instanceof ImagePreviewView) {
            selectImage((ImagePreviewView) event.getTarget());
        }
    }

    /**
     * set the active image (used for preview and setting start/end)
     *
     * @param imagePreviewView
     */
    public void selectImage(ImagePreviewView imagePreviewView) {
        imagePreview.setImage(new Image(imagePreviewView.getUrl()));
        activeIndex = imagePreviewView.getIndex();
    }

    /**
     * set active image as start of the time-lapse
     */
    public void setAsStart() {
        startIndex = activeIndex;
    }

    /**
     * set active image as start of the time-lapse
     */
    public void setAsEnd() {
        endIndex = activeIndex;
    }

    /**
     * convert the images into a time-lapse using FFmpegAdapter
     * and show a progress bar in a separate window
     *
     * @throws IOException
     */
    public void render() throws IOException {
        if (activeFileNames == null) {
            return;
        }

        int fps = (int) fpsSpinner.getValue();
        String[] toRender = Arrays.copyOfRange(activeFileNames, startIndex, endIndex + 1);

        // create progress indication window
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("loading.fxml"));
        Pane pane = fxmlLoader.load();
        LoadingController loadingController = (LoadingController) fxmlLoader.getController();

        Stage loadingStage = new Stage();
        loadingStage.setTitle("rendering");
        loadingStage.setResizable(false);
        loadingStage.setScene(new Scene(pane));

        // create the time-lapse
        FFmpegAdapter.render(toRender, fps, (progressInformation) -> {
            Platform.runLater(() -> {
                loadingController.setProgress(progressInformation.progress);
                loadingController.setText(progressInformation.text);
            });
            return null;
        });

        loadingStage.show();
    }

    /**
     * show an about dialog
     */
    public void showAbout() {
        AlertHelper.showInfo("About Timelapser", "Timelapser is a simple tool to create time-lapses.\n" +
                "More information: https://www.gitlab.com/cravay/timelapser");
    }

    /**
     * exit the program
     */
    public void exit() {
        Main.primaryStage.close();
    }
}
