package com.gmail.michaelschertenleib.timelapser;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * this class is used to save the url and index of every image in the picture-bar
 */
public class ImagePreviewView extends ImageView {
    /**
     * url to the image-file
     */
    private String url;

    /**
     * index in the picture-bar
     */
    private int index;

    /**
     * constructor
     *
     * @param url
     * @param index
     */
    public ImagePreviewView(String url, int index) {
        super(new Image(url, 0, 150, true, true, true));
        this.url = url;
        this.index = index;
    }

    /**
     * getter for url
     *
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * getter for index
     *
     * @return index
     */
    public int getIndex() {
        return index;
    }
}