# Timelapser

Timelapser is a simple GUI to create time-lapses. It uses [FFmpeg](https://www.ffmpeg.org/) under the hood.

## screenshot
![screenshot](./images/screenshot.png)

## example output
![example output](./images/output.gif)
